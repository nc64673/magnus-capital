Magnus Capital Equity Trading Platform

Provides an equity trading platform that enables the trader to buy and/or sell stocks and view their
transaction history

Team Members: Sarthak Bansal, Nicole Chow, Nivedha Mathiarasu, Jo Shi

Link to database schema diagram: https://app.sqldbm.com/SQLServer/Share/tvEeeLOhQy6bkpxzAeHQLEGFrngIE8md_DYjF4jNYw0#

See resources folder for project design (including UML and database schema)
