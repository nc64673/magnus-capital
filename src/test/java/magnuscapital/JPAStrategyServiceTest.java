package magnuscapital;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import magnuscapital.Trade;
import magnuscapital.JPATradeService;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import enums.Action;
import exceptions.NotFoundException;

public class JPAStrategyServiceTest {
	private EntityManagerFactory factory;
	private EntityManager em;
	private StrategyHandler handler;
	private JPAStrategyService service;
	
	@Before
	public void setManagers(){
		factory = Persistence.createEntityManagerFactory("PersistenceTest");
		em = factory.createEntityManager();
		handler = StrategyHandler.getInstance();
		service = new JPAStrategyService(factory);
	}

	// successfully add strategy
	@Test
	public void addStrategy() throws Exception {
		Strategy strategy = new Strategy("LPK", 1000);
		strategy.setId(10);
		service.add(strategy);
		assertNotNull(service.getByID(10));
		assertTrue(service.removeByID(10));
	}
	
	//unsuccessfully add Strategy
	@Test(expected=javax.persistence.PersistenceException.class)
	public void duplicateId() throws Exception{
		Strategy strategy = new Strategy("FAL", 200);
		strategy.setId(1);
		service.add(strategy);
		Strategy strategy2 = new Strategy("LPK", 1000);
		strategy2.setId(1);
		service.add(strategy2);
		assertTrue(service.removeByID(1));
	}

	// successfully remove Strategy by ID
	@Test(expected=NotFoundException.class)
	public void removeStrategyID() throws Exception {
		Strategy strategy = new Strategy("TBL", 300);
		strategy.setId(2);
		service.add(strategy);
		assertNotNull(service.getByID(2));
		service.removeByID(2);
		service.getByID(2);
	}
	
	//unsuccessfully remove Strategy by ID
	@Test(expected = NotFoundException.class)
	public void removeStrategyDNE() throws Exception{
		Strategy strategy = new Strategy("LVG", 1);
		strategy.setId(3);
		service.remove(strategy);
	}
	
	// successfully remove Strategy by Strategy
	@Test(expected=NotFoundException.class)
	public void removeStrategy() throws Exception {
		Strategy strategy = new Strategy("TBL", 100);
		strategy.setId(2);
		service.add(strategy);
		assertNotNull(service.getByID(2));
		service.remove(strategy);
		service.getByID(2);
	}

	//unsuccessfully remove trade by trade
	@Test(expected = NotFoundException.class)
	public void removeIdDNE() throws Exception{
		service.removeByID(3);
	}

	//trade DNE in db
	@Test(expected = NotFoundException.class)
	public void noId() throws Exception {
		service.getByID(3);
	}

	// test maxId
	@Test
	public void maxTradeId() throws Exception {
		Strategy strategy99 = new Strategy("TBL", 1000);
		strategy99.setId(99);
		service.add(strategy99);
		assertEquals(99, service.getMaxID());
		assertTrue(service.removeByID(99));
	}
	
	//test update trade in db
	@Test
	public void update() throws Exception{
		Strategy strategy = new Strategy("TBL", 1000);
		strategy.setId(4);
		service.add(strategy);
		Strategy x = service.getByID(4);
		x.setTicker("CHG");
		service.update(x);
		assertEquals("CHG", service.getByID(4).getTicker());	
		assertTrue(service.removeByID(4));
	}
	
	// test number of items
	@Test
	public void strategyCount() throws Exception {
		assertEquals(service.getCount(), service.getAll().size());
	}
	
	@After
	public void closeConnection() {
		factory.close();
	}


}
