package magnuscapital;


import java.util.ArrayList;

import StrategyAlgos.BollingerBands;
import StrategyAlgos.TwoMovingAvg;

public class BollingerBandsTest {
	public static void main(String [] args) throws Exception{
		StockInfo info = StockInfo.getInstance();
		
		System.out.println(info.getPricingData("http://incanada1.conygre.com:9080/prices", "AAPL", 5));
		ArrayList<Stock> current = info.queryStock("AAPL");			
		ArrayList<Double> lowPriceList = info.listLowPrices("AAPL");		
		ArrayList<Double> highPriceList = info.listHighPrices("AAPL");
		
		StrategyHandler handle = StrategyHandler.getInstance();
		BollingerBands bol = new BollingerBands(7,"AAPL",1000);
		
		handle.handler();
		
	}

}
