package magnuscapital;

import jms.MessageHandler;
import enums.Action;


public class MessageHandlerTest {
	
	private StrategyHandler handler = StrategyHandler.getInstance();
	private JPATradeService service;
	


	public void run() {
		Trade t = new Trade("TEAMF", 2000, 88.0,Action.BUY, StrategyType.TWOMA, new Strategy());
		Trade x = new Trade("TEAMF", 2000, 88.0,Action.SELL, StrategyType.BOLLINGER, new Strategy());
		
		handler.getTrades().add(t);
		handler.getTrades().add(x);
		MessageHandler.initiateTrade(t, "spring-beans.xml");
		MessageHandler.initiateTrade(x, "spring-beans.xml");
	}
	

	
	public static void main(String[] args) {
		MessageHandlerTest test = new MessageHandlerTest();
		test.run();
	}


}