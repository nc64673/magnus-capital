package magnuscapital;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import magnuscapital.Trade;
import magnuscapital.JPATradeService;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import enums.Action;
import exceptions.NotFoundException;

public class JPATradeServiceTest {
	private EntityManagerFactory factory;
	private EntityManager em;
	private StrategyHandler handler;
	private JPATradeService service;
	
	@Before
	public void setManagers(){
		factory = Persistence.createEntityManagerFactory("PersistenceTest");
		em = factory.createEntityManager();
		handler = StrategyHandler.getInstance();
		service = new JPATradeService(factory);
	}

	// successfully add trade
	@Test
	public void addTrade() throws Exception {
		Trade trade = new Trade("LPK", 1000, 1.55, Action.BUY, StrategyType.BOLLINGER, new Strategy());
		trade.setId(1);
		service.add(trade);
		assertNotNull(service.getByID(1));
		assertTrue(service.removeByID(1));
	}
	
	//unsuccessfully add trade
	@Test(expected=javax.persistence.PersistenceException.class)
	public void duplicateId() throws Exception{
		Trade trade = new Trade("LPK", 1000, 1.55, Action.BUY, StrategyType.BOLLINGER, new Strategy());
		trade.setId(1);
		service.add(trade);
		Trade trade2 = new Trade("LPK", 1000, 1.55, Action.BUY, StrategyType.BOLLINGER, new Strategy());
		trade2.setId(1);
		service.add(trade2);
		assertTrue(service.removeByID(1));
	}

	// successfully remove trade by ID
	@Test(expected=NotFoundException.class)
	public void removeTradeID() throws Exception {
		Trade trade = new Trade("TBL", 1000, 1.67, Action.SELL, StrategyType.TWOMA, new Strategy());
		trade.setId(2);
		service.add(trade);
		assertNotNull(service.getByID(2));
		assertTrue(service.removeByID(2));
		service.getByID(2);
	}
	
	//unsuccessfully remove trade by ID
	@Test(expected = NotFoundException.class)
	public void removeTradeDNE() throws Exception{
		Trade trade = new Trade("LPK", 1000, 1.55, Action.BUY, StrategyType.BOLLINGER, new Strategy());
		trade.setId(3);
		service.remove(trade);
	}
	
	// successfully remove trade by trade
	@Test(expected=NotFoundException.class)
	public void removeTrade() throws Exception {
		Trade trade = new Trade("TBL", 1000, 1.67, Action.SELL, StrategyType.TWOMA, new Strategy());
		trade.setId(2);
		service.add(trade);
		assertNotNull(service.getByID(2));
		service.remove(trade);
		service.getByID(2);
	}

	//unsuccessfully remove trade by trade
	@Test(expected = NotFoundException.class)
	public void removeIdDNE() throws Exception{
		service.removeByID(3);
	}

	//trade DNE in db
	@Test(expected = NotFoundException.class)
	public void noId() throws Exception {
		service.getByID(3);
	}

	// test maxId
	@Test
	public void maxTradeId() throws Exception {
		Trade trade99 = new Trade("TBL", 1000, 1.67, Action.SELL, StrategyType.TWOMA, new Strategy());
		trade99.setId(99);
		service.add(trade99);
		assertEquals(99, service.getMaxID());
		assertTrue(service.removeByID(99));
	}
	
	//test update trade in db
	@Test
	public void update() throws Exception{
		Trade trade = new Trade("TBL", 1000, 1.67, Action.SELL, StrategyType.TWOMA, new Strategy());
		trade.setId(4);
		service.add(trade);
		Trade x = service.getByID(4);
		x.setStrategyType(StrategyType.BOLLINGER);
		service.update(x);
		assertEquals(StrategyType.BOLLINGER, service.getByID(4).getStrategyType());
		assertTrue(service.removeByID(4));
	}
	
	// test number of items
	@Test
	public void tradeCount() throws Exception {
		assertEquals(service.getCount(), service.getAll().size());
	}
	
	@After
	public void closeConnection() {
		factory.close();
	}


}
