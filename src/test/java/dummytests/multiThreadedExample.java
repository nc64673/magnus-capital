package dummytests;
import java.util.concurrent.*;

public class multiThreadedExample {

		 
	    public static void main (String[] args) {
	 
	        ExecutorService pool = Executors.newCachedThreadPool();
	        System.out.println("pools created");
	        pool.execute(new clocl("A"));
	        pool.execute(new clocl("B"));
	        pool.execute(new clocl("C"));
	        pool.execute(new clocl("D"));
	 
	        pool.shutdown();
	 
	    }
}
