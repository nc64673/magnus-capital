package magnuscapital;

public class Stock {
	
	private String ticker;
	private String timeStamp;
	private Double open;
	private Double high;
	private Double low;
	private Double close;
	private int volume;
	
	// Empty constructor
	public Stock() {
		
	}
	
	// Constructor with parameters
	public Stock(String ticker, String timeStamp, double open, double high, double low, double close, int volume) {
		this.setTicker(ticker);
		this.setTimeStamp(timeStamp);
		this.setOpen(open);
		this.setHigh(high);
		this.setLow(low);
		this.setClose(close);
		this.setVolume(volume);
	}

	
	/** GETTERS AND SETTERS **/
	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}


	public Double getOpen() {
		return open;
	}

	public void setOpen(Double open) {
		this.open = open;
	}

	public Double getClose() {
		return close;
	}

	public void setClose(Double close) {
		this.close = close;
	}

	public Double getHigh() {
		return high;
	}

	public void setHigh(Double high) {
		this.high = high;
	}

	public Double getLow() {
		return low;
	}

	public void setLow(Double low) {
		this.low = low;
	}	

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

}
