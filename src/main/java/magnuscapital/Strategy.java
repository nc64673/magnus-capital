package magnuscapital;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Id;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.persistence.Transient;

import enums.Action;
import enums.StrategyPosition;
import enums.message;
import jms.MessageHandler;


import java.io.Serializable;

import javax.persistence.Column;

@SuppressWarnings("serial")
@Entity
@Table(name="Strategies")
public class Strategy implements Serializable {
	
	@Id
	@Column(name="ID")
	private int id;
	
	@Column(name="Ticker")
	protected String ticker;
	
	@Column(name="Number_of_Shares")
	protected long shares;
	
	@Column(name="Strategy")
	protected StrategyType type;
	
	private message BrokerMessage;	
	private double lastOpenPrice;
	private long currentShares;	
	private StrategyPosition currentPosition;	
	private Action currentAction;
	
	
	
	public Action getCurrentAction() {
		return currentAction;
	}

	public void setCurrentAction(Action currentAction) {
		this.currentAction = currentAction;
	}

	public StrategyPosition getCurrentPosition() {
		return currentPosition;
	}

	public void setCurrentPosition(StrategyPosition currentPosition) {
		this.currentPosition = currentPosition;
	}

	public long getCurrentShares() {
		return currentShares;
	}

	public void setCurrentShares(long currentShares) {
		this.currentShares = currentShares;
	}

	public message getBrokerMessage() {
		return BrokerMessage;
	}

	public void setBrokerMessage(message brokerMessage) {
		BrokerMessage = brokerMessage;
	}

	public double getLastOpenPrice() {
		return lastOpenPrice;
	}

	public void setLastOpenPrice(double lastOpenPrice) {
		this.lastOpenPrice = lastOpenPrice;
	}

	public Strategy() {
		
	}

	@SuppressWarnings("deprecation")
	public Strategy(String ticker, long shares) {
		id = new Integer(StrategyHandler.getInstance().getStrategyId());
		StrategyHandler.getInstance().incStrategyId();
		this.ticker = ticker;
		this.shares = shares;

	}
	public void MessageInterpreter() {
		System.out.println("Starting MessageInterpreter");
		if (currentPosition.equals(StrategyPosition.OPENING) && BrokerMessage.equals(message.APPROVED)) {
			currentPosition = StrategyPosition.OPEN;
			System.out.println("Opened from approved");
		}
		else if(currentPosition.equals(StrategyPosition.OPENING) && BrokerMessage.equals(message.DECLINED)) {
			currentPosition = StrategyPosition.CLOSE;
			System.out.println("closed from rejection");
		}
		else if (currentPosition.equals(StrategyPosition.OPENING) && BrokerMessage.equals(message.PARTIAL_FILL)) {
			currentPosition = StrategyPosition.OPEN;
			System.out.println("Opened from partialfill");
			Trade newTrade = null;
			if (currentAction.equals(Action.LONG)) {
				newTrade = new Trade(ticker, currentShares , lastOpenPrice,Action.SELL, StrategyType.TWOMA, this);
			}
			else {
				newTrade = new Trade(ticker, currentShares , lastOpenPrice,Action.BUY, StrategyType.TWOMA, this);
			}
			StrategyHandler.getInstance().getTrades().add(newTrade);
			MessageHandler.initiateTrade(newTrade, "spring-beans.xml");
		}
		else {
			System.out.println("FAILING???");
		}
	}

	
	public int getStrategyId() {
		return id;
	}

	public Double calculateProfit() {
		return null;
	}
	

	public Double calculateROI() {
		return null;
	}

	public void addDB() {

	}
	public void run() {
		 String threadName = Thread.currentThread().getName();
		 System.out.println(threadName + "HMM");
	}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the ticker
	 */
	public String getTicker() {
		return ticker;
	}


	/**
	 * @param ticker the ticker to set
	 */
	public void setTicker(String ticker) {
		this.ticker = ticker;
	}


	/**
	 * @return the shares
	 */
	public long getShares() {
		return shares;
	}


	/**
	 * @param shares the shares to set
	 */
	public void setShares(long shares) {
		this.shares = shares;
	}


	/**
	 * @return the type
	 */
	public StrategyType getType() {
		return type;
	}


	/**
	 * @param type the type to set
	 */
	public void setType(StrategyType type) {
		this.type = type;
	}
	
	

}
