package magnuscapital;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Id;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.tuple.entity.EntityMetamodel.GenerationStrategyPair;

import StrategyAlgos.MovingAverage;
import enums.Action;
import enums.StrategyPosition;
import enums.message;
import jms.MessageHandler;


import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.Column;

@SuppressWarnings("serial")
@Entity
@Table(name="newStrategies")
public class newStrategy implements Serializable{
	@Id
	@Column(name="ID")
	private int id;
	
	@Column(name="Ticker")
	private String ticker;
	
	@Column(name="Number_of_Shares")
	private long shares;
	
	@Column(name="Strategy")
	protected StrategyType type;
	
	@Column(name="longAverage")
	private double longAvg;
	
	@Column(name="shortAverage")
	private double shortAvg;
	
	@Column(name="longPeriod")
	private int longPeriod;
	
	@Column(name="shortPeriod")
	private int shortPeriod;
	
	@Transient
	private MovingAverage l;
	
	@Transient
	private MovingAverage s;
	
	@Column(name= "BBperiod")
	private int period;
	
	@Column(name="mean")
	private double mean;
	
	@Transient
	private message BrokerMessage;	
	@Transient
	private double lastOpenPrice;
	@Transient
	private long currentShares;	
	@Transient
	private StrategyPosition currentPosition;	
	@Transient
	private Action currentAction;
	
	//Call this for TWOMA
	public newStrategy() {
		
	}
	public newStrategy(String Ticker, long Shares, StrategyType stype,int longPeriod, int shortPeriod,int period) {
		id = new Integer(StrategyHandler.getInstance().getStrategyId());
		StrategyHandler.getInstance().incStrategyId();
		this.ticker = ticker;
		this.shares = shares;
		this.type = stype;
		if(stype.equals(StrategyType.TWOMA)) {
			this.longPeriod = longPeriod;
			this.shortPeriod = shortPeriod;
			longAvg = 0;
			shortAvg = 0;
			l = new MovingAverage(longPeriod);
			s = new MovingAverage(shortPeriod);		
			period = 0;	
			
		}
		else if (stype.equals(StrategyType.BOLLINGER)) {
			longPeriod = 0;
			shortPeriod = 0;
			l = null;
			s = null;
			currentPosition = StrategyPosition.CLOSE;
			this.period = period;			
		}
		longAvg = 0;
		shortAvg = 0;
		mean = 0;
		currentPosition = StrategyPosition.CLOSE;
		StrategyHandler.getInstance().addToListnew(this);
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("PersistenceTest");
		EntityManager em = factory.createEntityManager();

		JPAStrategyService service = new JPAStrategyService(factory);
		
		System.out.println("HMMM intresting");
		
		service.addNew(this);
	}
	
	public void TwoMAExecute(ArrayList<Double>lowPriceList, ArrayList<Double>highPriceList ) {
		for (int i = 0;i<lowPriceList.size();i++) {
			double  stockPrice = (lowPriceList.get(i) + highPriceList.get(i))/2;  
			l.addData(stockPrice);
			s.addData(stockPrice);
			longAvg  = l.getMean();
			shortAvg = s.getMean();
//			System.out.println(longAvg + " " + shortAvg);
			
			if (getCurrentPosition().equals(StrategyPosition.CLOSE)) {
				System.out.println("Currently closed");
				if(shortAvg>longAvg) {
					
					setCurrentPosition(StrategyPosition.OPENING); 
					System.out.println("Opening short");
					Trade newTrade = new Trade(ticker, shares, stockPrice, Action.BUY, StrategyType.TWOMA, this);
					StrategyHandler.getInstance().getTrades().add(newTrade);					
					MessageHandler.initiateTrade(newTrade, "spring-beans.xml");
					setLastOpenPrice(stockPrice);
					System.out.println("BUY");
					setCurrentAction(Action.LONG);
		
				} else if (shortAvg<longAvg) {
					
					setCurrentPosition(StrategyPosition.OPENING); 
					System.out.println("Opening long");
					Trade newTrade = new Trade(ticker, shares, stockPrice, Action.SELL, StrategyType.TWOMA, this);
					StrategyHandler.getInstance().getTrades().add(newTrade);
					MessageHandler.initiateTrade(newTrade, "spring-beans.xml");
					setLastOpenPrice(stockPrice);
					System.out.println("SELL");
					setCurrentAction(Action.SHORT);
					
				}
				else {
					System.out.println("NO POSITION");
				}
			}
			else if (getCurrentPosition().equals(StrategyPosition.OPEN)) {
				System.out.println("currently Open");				
				double percentage = ((i-getLastOpenPrice())/getLastOpenPrice())*100 ;
				// if price is in between the threshold values 
				if (percentage > 30 || percentage < -30  ) {
					setCurrentPosition(StrategyPosition.CLOSE);
					System.out.println("CLOSED If open");
				}

			}
	
		}
		
	}
	
	
	
	public void BBExecute (ArrayList<Double>lowPriceList, ArrayList<Double>highPriceList) {
		double total_average = 0;
		ArrayList<Double> temp = new ArrayList<Double>();
		for (int i = 0; i < lowPriceList.size(); i++) {
			
			double stockPrice = (lowPriceList.get(i) + highPriceList.get(i))/2; 
			temp.add(stockPrice);
			total_average += stockPrice;
			if(i >= period - 1) {
				double total_bollinger = 0;
                double average = total_average / period;
                
                for (int x = i; x > (i - period); x--) {
                    total_bollinger += Math.pow((temp.get(x) - average), 2);
                }
                double stdev = Math.sqrt(total_bollinger / period);
                double h = (stockPrice - mean)/stdev;
                
                if (getCurrentPosition().equals(StrategyPosition.CLOSE)) {
                	System.out.println("Currently closed");
                	if(h < -2) {
                		setCurrentPosition(StrategyPosition.OPENING); 
                    	Trade newTrade = new Trade(ticker, shares, stockPrice, Action.BUY, StrategyType.BOLLINGER, this);
        				StrategyHandler.getInstance().getTrades().add(newTrade);
        				MessageHandler.initiateTrade(newTrade, "spring-beans.xml");
        				setLastOpenPrice(stockPrice);
                    	System.out.println("BUY");
                    	setCurrentAction(Action.LONG);
                    }else if(h > 2) {
                    	setCurrentPosition(StrategyPosition.OPENING); 
                    	Trade newTrade = new Trade(ticker, shares, stockPrice, Action.SELL, StrategyType.BOLLINGER, this);
        				StrategyHandler.getInstance().getTrades().add(newTrade);
        				MessageHandler.initiateTrade(newTrade, "spring-beans.xml");
        				setLastOpenPrice(stockPrice);
                    	System.out.println("SELL");
                    	setCurrentAction(Action.SHORT);
                    }
                }
                else if (getCurrentPosition().equals(StrategyPosition.OPEN)) {
    				System.out.println("currently Open");				
    				double percentage = ((i-getLastOpenPrice())/getLastOpenPrice())*100 ;
    				// if price is in between the threshold values 
    				if (percentage > 30 || percentage < -30  ) {
    					setCurrentPosition(StrategyPosition.CLOSE);
    				}

    			}
    			else {
    				System.out.println("NO POSITION");
    			}
			}
		}
		System.out.println("NONE");
	}
	
	
	public void execute(ArrayList<Double>lowPriceList, ArrayList<Double>highPriceList) {
		if(getType().equals(StrategyType.TWOMA)) {
			TwoMAExecute(lowPriceList, highPriceList);
		}
		else if(getType().equals(StrategyType.BOLLINGER)) {
			BBExecute(lowPriceList, highPriceList);
		}
		else {
			System.out.println("NOT A VALID TYPE PLEASE ENTER A VALID TYPE");
		}
	}
	
	public void MessageInterpreter() {
		System.out.println("Starting MessageInterpreter");
		if (currentPosition.equals(StrategyPosition.OPENING) && BrokerMessage.equals(message.APPROVED)) {
			currentPosition = StrategyPosition.OPEN;
			System.out.println("Opened from approved");
		}
		else if(currentPosition.equals(StrategyPosition.OPENING) && BrokerMessage.equals(message.DECLINED)) {
			currentPosition = StrategyPosition.CLOSE;
			System.out.println("closed from rejection");
		}
		else if (currentPosition.equals(StrategyPosition.OPENING) && BrokerMessage.equals(message.PARTIAL_FILL)) {
			currentPosition = StrategyPosition.OPEN;
			System.out.println("Opened from partialfill");
			Trade newTrade = null;
			if (currentAction.equals(Action.LONG)) {
				newTrade = new Trade(ticker, currentShares , lastOpenPrice,Action.SELL, StrategyType.TWOMA, this);
			}
			else {
				newTrade = new Trade(ticker, currentShares , lastOpenPrice,Action.BUY, StrategyType.TWOMA, this);
			}
			StrategyHandler.getInstance().getTrades().add(newTrade);
			MessageHandler.initiateTrade(newTrade, "spring-beans.xml");
		}
		else {
			System.out.println("FAILING???");
		}
	}

	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTicker() {
		return ticker;
	}
	public void setTicker(String ticker) {
		this.ticker = ticker;
	}
	public long getShares() {
		return shares;
	}
	public void setShares(long shares) {
		this.shares = shares;
	}
	public StrategyType getType() {
		return type;
	}
	public void setType(StrategyType type) {
		this.type = type;
	}
	public double getLongAvg() {
		return longAvg;
	}
	public void setLongAvg(double longAvg) {
		this.longAvg = longAvg;
	}
	public double getShortAvg() {
		return shortAvg;
	}
	public void setShortAvg(double shortAvg) {
		this.shortAvg = shortAvg;
	}
	public int getLongPeriod() {
		return longPeriod;
	}
	public void setLongPeriod(int longPeriod) {
		this.longPeriod = longPeriod;
	}
	public int getShortPeriod() {
		return shortPeriod;
	}
	public void setShortPeriod(int shortPeriod) {
		this.shortPeriod = shortPeriod;
	}
	public MovingAverage getL() {
		return l;
	}
	public void setL(MovingAverage l) {
		this.l = l;
	}
	public MovingAverage getS() {
		return s;
	}
	public void setS(MovingAverage s) {
		this.s = s;
	}
	public int getPeriod() {
		return period;
	}
	public void setPeriod(int period) {
		this.period = period;
	}
	public double getMean() {
		return mean;
	}
	public void setMean(double mean) {
		this.mean = mean;
	}
	public message getBrokerMessage() {
		return BrokerMessage;
	}
	public void setBrokerMessage(message brokerMessage) {
		BrokerMessage = brokerMessage;
	}
	public double getLastOpenPrice() {
		return lastOpenPrice;
	}
	public void setLastOpenPrice(double lastOpenPrice) {
		this.lastOpenPrice = lastOpenPrice;
	}
	public long getCurrentShares() {
		return currentShares;
	}
	public void setCurrentShares(long currentShares) {
		this.currentShares = currentShares;
	}
	public StrategyPosition getCurrentPosition() {
		return currentPosition;
	}
	public void setCurrentPosition(StrategyPosition currentPosition) {
		this.currentPosition = currentPosition;
	}
	public Action getCurrentAction() {
		return currentAction;
	}
	public void setCurrentAction(Action currentAction) {
		this.currentAction = currentAction;
	}
	
	
	
}
