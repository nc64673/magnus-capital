package magnuscapital;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import StrategyAlgos.TwoMovingAvg;
import enums.Trigger;

public class StrategyHandler {
	private ArrayList<Trade> trades;
	private Queue<Strategy> strategies;
	private Queue<newStrategy> nstrategies;
	private static StrategyHandler singleInstance;
	private int correlationId;
	private int strategyId;

	private StrategyHandler() {
		trades = new ArrayList<Trade>();
		strategies = new LinkedList<Strategy>();
		nstrategies = new LinkedList<newStrategy>();

		EntityManagerFactory f1 = Persistence.createEntityManagerFactory("PersistenceTest");
		EntityManager em1 = f1.createEntityManager();
		JPATradeService service1 = new JPATradeService(f1);
		
		if(service1.getCount() == 0) {
			correlationId = 1;
		} else {
			correlationId = service1.getMaxID() + 1;
		}
		
		EntityManagerFactory f2 = Persistence.createEntityManagerFactory("PersistenceTest");
		EntityManager em2 = f2.createEntityManager();
		JPATradeService service2 = new JPATradeService(f2);
		
		if(service2.getCount() == 0) {
			strategyId = 1;
		} else {
			strategyId = service2.getMaxID() + 1;
		}
		
	}

	public static StrategyHandler getInstance() {
		if (singleInstance == null) {
			singleInstance = new StrategyHandler();
		}
		return singleInstance;
	}

	public Trigger getTrigger() {
		return Trigger.CONTINUE;
	}
	
	public void incCorelationid() {
		correlationId++;
	}
	
	public void incStrategyId() {
		strategyId++;
	}
	
	public int getStrategyId() {
		return strategyId;
	}
	
	public int getCorrelationId() {
		return correlationId;
	}

	public List<Trade> getTrades() {
		return trades;
	}

	public String getTicker() {
		return "AAPL";
	}
	
	public void addToList(Strategy s) {
		strategies.add(s);
	}
	public void addToListnew(newStrategy s) {
		nstrategies.add(s);
	}
	public void handler() {
		Trigger t;
		ArrayList<Double>lowPriceList ;
		ArrayList<Double>highPriceList ;
		do {
//			System.out.println("Entered handler");
			t = getTrigger();
			
			lowPriceList = StockInfo.getInstance().listLowPrices(getTicker());
			highPriceList = StockInfo.getInstance().listHighPrices(getTicker());
//			System.out.println(lowPriceList);
			
//			System.out.println(strategies.size());
			for (newStrategy s : nstrategies)
				if (s.type.equals(StrategyType.TWOMA)) {
	
					s.execute(lowPriceList, highPriceList);
				} try {
				System.out.println("sleeping");
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} while (t.equals(Trigger.CONTINUE));

		if (t.equals(Trigger.THRESHOLD)) {
			System.out.println("Thersold limit reached");
			return;

		} else if (t.equals(Trigger.USER_INTERRUPT)) {
			System.out.println("User interrupted");
			return;
		}
	}

}
