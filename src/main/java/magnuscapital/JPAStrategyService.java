package magnuscapital;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import exceptions.ConflictException;
import exceptions.NotFoundByNumberException;
import exceptions.NotFoundException;

public class JPAStrategyService {
	
	private EntityManagerFactory emf;
	private String getAllQuery;
	private String getCountQuery;
	private String getMaxQuery;
	private static String GET_BY_NUMBER = "select i from Strategy i where i.number = ?1";

	public JPAStrategyService() {
		
	}
	
	public JPAStrategyService(EntityManagerFactory emf) {
		this.emf = emf;
		getAllQuery = "select x from Strategy x";
		getCountQuery = "select count(x) from Strategy x";
		getMaxQuery = "select max(x.id) from Strategy x";
	}

	/**
	 * Helper to reject operations on entities that don't yet exist.
	 */
	protected Strategy findOrFail(EntityManager em, Strategy entity) {
		Object ID = em.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(entity);
		Strategy result = em.find(Strategy.class, ID);
		if (result == null)
			throw new NotFoundException(Strategy.class, ID);
		return result;
	}

	/**
	 * Returns the total number of objects.
	 */
	public long getCount() {
		EntityManager em = emf.createEntityManager();
		long result = -1;
		try {
			em.getTransaction().begin();
			result = em.createQuery(getCountQuery, Long.class).getSingleResult();
			em.getTransaction().commit();
		} catch (PersistenceException ex) {
			ex.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return result;
	}
	
	public int getMaxID() {
		EntityManager em =  emf.createEntityManager();
		int result = -1;
		try {
			em.getTransaction().begin();
			result = em.createQuery(getMaxQuery,Integer.class).getSingleResult();
		} catch (PersistenceException ex) {
			ex.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return result;
	}
	
	/**
	 * Returns a list of all entities of our managed type.
	 */
	@SuppressWarnings("unchecked")
	public List<Strategy> getAll() {
		EntityManager em = emf.createEntityManager();
		List<Strategy> result = null;
		try {
			em.getTransaction().begin();
			result = em.createQuery(getAllQuery).getResultList();
			em.getTransaction().commit();
		} catch (PersistenceException ex) {
			ex.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return result;
	}

	/**
	 * Returns the entity with the given ID, or null if not found.
	 */
	public Strategy getByID(int ID) throws NotFoundException {
		EntityManager em = emf.createEntityManager();
		Strategy result = null;
		try {
			em.getTransaction().begin();
			result = em.find(Strategy.class, ID);
			em.getTransaction().commit();
		} catch (PersistenceException ex) {
			ex.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			em.close();
		}
		if (result == null)
			throw new NotFoundException(Strategy.class, ID);
		return result;
	}

	/**
	 * Adds the given object to the database, with a generated ID that is
	 * guaranteed to be unique.
	 */
	public Strategy add(Strategy newObject) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(newObject);
			em.getTransaction().commit();
		} catch (PersistenceException ex) {
			ex.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			throw new PersistenceException();
		} finally {
			em.close();
		}
		return newObject;
	}
	
	public newStrategy addNew(newStrategy newObject) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(newObject);
			em.getTransaction().commit();
		} catch (PersistenceException ex) {
			ex.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return newObject;
	}

	/**
	 * Adds the given object to the database, with given "natural" ID.
	 */
	public void add(int ID, Strategy newObject) throws ConflictException {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			Strategy check = em.find(Strategy.class, ID);

			if (check != null)
				throw new ConflictException(newObject, ID);
			em.persist(newObject);
		} catch (PersistenceException ex) {
			ex.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			em.close();
		}
	}

	/**
	 * Merges the given object into the database.
	 */
	public Strategy update(Strategy modifiedObject) throws NotFoundException {
		EntityManager em = emf.createEntityManager();
		Strategy result = null;
		try {
			em.getTransaction().begin();
			findOrFail(em, modifiedObject);
			result = em.merge(modifiedObject);
			em.getTransaction().commit();
		} catch (PersistenceException ex) {
			ex.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return result;
	}

	/**
	 * Removes the given object from the database.
	 */
	public boolean remove(Strategy oldObject) throws NotFoundException {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.remove(findOrFail(em, oldObject));
			em.getTransaction().commit();
			return true;
		} catch (PersistenceException ex) {
			ex.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			return false;
		} finally {
			em.close();
		}
	}

	/**
	 * Removes the object with the given ID from the database.
	 */
	public boolean removeByID(int ID) throws NotFoundException {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			Strategy doomed = em.find(Strategy.class, ID);
			if (doomed == null)
				throw new NotFoundException(Strategy.class, ID);
			em.remove(doomed);
			em.getTransaction().commit();
			return true;
		} catch (PersistenceException ex) {
			ex.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			return false;
		} finally {
			em.close();
		}
	}

	/**
	 * Get all Strategy, ordered by number.
	 */
	public List<Strategy> getAllByNumber() {
		EntityManager em = emf.createEntityManager();
		List<Strategy> result = null;
		try {
			em.getTransaction().begin();
			result = em.createQuery("select i from Strategy i order by i.ID", Strategy.class).getResultList();
			em.getTransaction().commit();
		} catch (PersistenceException ex) {
			ex.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			em.close();
		}

		return result;
	}

	/**
	 * Gets the Person with the given number, or null if not found.
	 */
	public Strategy getByNumber(int PersonNumber) throws NotFoundByNumberException {
		EntityManager em = emf.createEntityManager();
		Strategy result = null;
		try {
			em.getTransaction().begin();
			result = em.createQuery(GET_BY_NUMBER, Strategy.class).setParameter(1, PersonNumber).getSingleResult();
			em.getTransaction().commit();
		} catch (NoResultException ex) {
			em.getTransaction().rollback();
			throw new NotFoundByNumberException(PersonNumber);
		} catch (PersistenceException ex) {
			ex.printStackTrace();
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
		} finally {
			em.close();
		}
		return result;
	}

}
