package magnuscapital;

import java.io.BufferedReader;
import java.net.*;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;   

public class StockInfo {
	
	// Hashmap that holds the ticker and it's prices
	private  HashMap<String,ArrayList<Stock>> currentPrices;
	
	private static ArrayList<Stock> stocks;
	
	// Flag that checks if the data from the REST API is a header line or line that contains prices
	private static Boolean headerLine = false;
	
	// Singleton class
	private static StockInfo singleInstance = null;
	
	// Constructor for singleton class
	private StockInfo() {
		currentPrices  = new HashMap<String, ArrayList<Stock>>();	
		stocks = new ArrayList<Stock>();
	}
	
	// Single instance for the class
	public static StockInfo getInstance() {
		if(singleInstance == null) {
			singleInstance = new StockInfo();		
		}
		return singleInstance;
	}

	// Populates the hashmap with ticker and it's price values
	public  Map<String, ArrayList<Stock>> populateMap(String ticker, String priceLine) {
		
		String split = ",";
		
		try {
			// Data line from the REST API
			String[] value = priceLine.split(split); 

			//Check if the data line is header and set flag value accordingly
			if (value[0].equals("timestamp") && value[1].equals("open") && value[2].equals("high") &&
					value[3].equals("low") && value[4].equals("close") && value[5].equals("volume")) {
				
				StockInfo.getInstance().headerLine = true;			
			}
			else {
				StockInfo.getInstance().headerLine = false;
			}
			
			System.out.println("priceLine: " + priceLine);
			
			// If header line, do not store in hashmap
			if (StockInfo.getInstance().headerLine == true) {
				System.out.println("HEADERLINE = TRUE : DO NOT STORE IN HASHMAP");
			}
			// If price line, store in hashmap
			else {
				System.out.println("HEADERLINE = FALSE");			
				
				// Parse the data line to obtain timestamp, open/close/low/high/volume values
				String timeStamp = value[0];
				double open = Double.parseDouble(value[1]);
				double high = Double.parseDouble(value[2]);
				double low = Double.parseDouble(value[3]);
				double close  = Double.parseDouble(value[4]);
				int volume = Integer.parseInt(value[5]);
				
				// Create Stock object with parsed data
				Stock stock = new Stock(ticker, timeStamp, open, high, low, close, volume);
				
				/*System.out.println("timeStamp: " + stock.getTimeStamp());
				System.out.println("open: " + stock.getOpen());
				System.out.println("high: " + stock.getHigh());
				System.out.println("low: " + stock.getLow());
				System.out.println("close: " + stock.getClose());
				System.out.println("volume: " + stock.getVolume());*/
				
				// Add stock to list of stocks
				StockInfo.getInstance().stocks.add(stock);
				
				/*for(Stock s: stocks) {
					    System.out.println(s.getOpen());  
				}*/
				
				// Store ticker and its list of stock values in hashmap
				StockInfo.getInstance().currentPrices.put(ticker, StockInfo.getInstance().stocks);			
			}
				
		}catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return StockInfo.getInstance().currentPrices; 
	}
	
	// Obtain list of prices from hashmap given the ticker 
	public ArrayList<Stock> queryStock(String ticker) {
		if(currentPrices.isEmpty()) {					
			System.out.println("Hashmap empty: no prices available");			
			return null;
		}
		else if(currentPrices.containsKey(ticker)) {		
			ArrayList<Stock> result = new ArrayList<Stock>();			
			result = currentPrices.get(ticker);			
			System.out.println("TICKER: " + ticker);			
			for (Stock s: result) {
				System.out.println("TIMESTAMP: " + s.getTimeStamp());
				System.out.println("OPEN: " + s.getOpen()); 
				System.out.println("HIGH: " + s.getHigh()); 
				System.out.println("LOW: " + s.getLow()); 
				System.out.println("CLOSE: " + s.getClose()); 
				System.out.println("VOLUME: " + s.getVolume()); 			
			}				
			return result;
		}
		else{
			return null;
		}
	}
	
	public ArrayList<Double> listLowPrices(String ticker){
		
		ArrayList<Double> list = new ArrayList<Double>();		
		if(currentPrices.containsKey(ticker)) {			
			ArrayList<Stock> result = new ArrayList<Stock>();			
			result = currentPrices.get(ticker);			
//			System.out.println("TICKER: " + ticker);		
			for (Stock s: result) { 				
				list.add(s.getLow());			
//				System.out.println("LOW: " + s.getLow()); 						
			}
//			System.out.println();
		}		
//		for(int i = 0; i < list.size(); i++) {
////			System.out.println(list.get(i));		
//		}		
		return list;		
	}
	
	public ArrayList<Double> listHighPrices(String ticker){
		ArrayList<Double> list = new ArrayList<Double>();		
		if(currentPrices.containsKey(ticker)) {
			ArrayList<Stock> result = new ArrayList<Stock>();
			result = currentPrices.get(ticker);
//			System.out.println("TICKER: " + ticker);
			for (Stock s: result) { 
				list.add(s.getHigh());
//				System.out.println("HIGH: " + s.getHigh()); 			
			}		
		}
		return list;		
	}
	
	// Get pricing data from REST API given the ticker and periods
	public Boolean getPricingData(String priceURL, String tickerSymbol, int periods) throws Exception {
	      //StringBuilder result = new StringBuilder();
	      priceURL = priceURL + "/" + tickerSymbol + "?" + "periods=" + periods;
	      URL url = new URL(priceURL);
	      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	      conn.setRequestMethod("GET");    
		  BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));     
	      String line;
          
	      // For each line of data from REST API, populate hash map
	      while ((line = rd.readLine()) != null){
	          //result.append(line);
	          StockInfo.getInstance().populateMap(tickerSymbol, line);
          }
          rd.close();
          return true;
	}


}

