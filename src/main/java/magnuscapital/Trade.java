package magnuscapital;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import enums.Action;

import java.io.Serializable;

import javax.persistence.Column;

@SuppressWarnings("serial")
@Entity
@Table(name="Trades")
public class Trade implements Serializable {
	
	@Id
	@Column(name="ID")
	private int id;
	
	@Column(name="Ticker")
	private String ticker;
	
	@Column(name="Shares")
	private long numShares;
	
	@Column(name="Price")
	private double price;
	
	@Column(name="Action")
	private Action action;
	
	@Column(name="StrategyType")
	private StrategyType strategyType;	
	
	@ManyToOne
	@JoinColumn(name = "Strategy")
	private Strategy strategy;

	public Trade() {
	}
	
	public Trade(String ticker, long shares, double price, Action action, StrategyType strategyType, Strategy strategy) {
		setId(new Integer(StrategyHandler.getInstance().getCorrelationId()));
		StrategyHandler.getInstance().incCorelationid();
		setTicker(ticker);
		setNumShares(shares);
		setPrice(price);
		setAction(action);
		setStrategyType(strategyType);
		setStrategy(strategy);
	}
	public Trade(String ticker, long shares, double price, Action action, StrategyType strategyType, newStrategy strategy) {
		setId(new Integer(StrategyHandler.getInstance().getCorrelationId()));
		StrategyHandler.getInstance().incCorelationid();
		setTicker(ticker);
		setNumShares(shares);
		setPrice(price);
		setAction(action);
		setStrategyType(strategyType);
		setStrategy(strategy);
	}
	private void setStrategy(newStrategy strategy2) {
		// TODO Auto-generated method stub
		
	}

	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public long getNumShares() {
		return numShares;
	}

	public void setNumShares(long numShares) {
		this.numShares = numShares;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public StrategyType getStrategyType() {
		return strategyType;
	}

	public void setStrategyType(StrategyType strategyType) {
		this.strategyType = strategyType;
	}
	
	public Strategy getStrategy() {
		return strategy;
	}

	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}

	public Double calculateProfit() {
		return null;
	}

	public Double calculateROI() {
		return null;
	}

}
