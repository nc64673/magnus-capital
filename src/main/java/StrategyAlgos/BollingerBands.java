package StrategyAlgos;


import java.util.ArrayList;
import java.util.Arrays;

import jms.MessageHandler;
import enums.Action;
import enums.StrategyPosition;
import magnuscapital.StockInfo;
import magnuscapital.Strategy;
import magnuscapital.StrategyHandler;
import magnuscapital.StrategyType;
import magnuscapital.Trade;



public class BollingerBands extends Strategy {
	
	private MovingAverage movingAverage;
	private int period;
	private double mean;
	private double std;
	private double price;
	
//	public static void main(String[] args){
//		// BollingerBands bb = new BollingerBands(7,"APPL",1000);
//		BollingerBands bb = new BollingerBands();
//		ArrayList<Double> low = new ArrayList<Double>();
//		low.add(2.5);
//		low.add(3.0);
//		low.add(4.0);
//		low.add(5.0);
//		low.add(6.0);
//		low.add(4.0);
//		low.add(2.0);
//		ArrayList<Double> high = new ArrayList<Double>();
//		high.add(3.0);
//		high.add(4.0);
//		high.add(5.0);
//		high.add(6.0);
//		high.add(7.0);
//		high.add(5.0);
//		high.add(3.0);
//		
//		System.out.println(low.size());
//		
//		bb.excute(low, high);
//		
//	}
	
	
	public BollingerBands(int period, String ticker, long shares) {
		super(ticker, shares);
		super.type = StrategyType.BOLLINGER;
		
		this.period = period;
		// movingAverage = new MovingAverage(period);
		
		super.setCurrentPosition(StrategyPosition.CLOSE);
		
		StrategyHandler.getInstance().addToList(this);
	}
	
	public void excute (ArrayList<Double>lowPriceList, ArrayList<Double>highPriceList) {
		double total_average = 0;
		ArrayList<Double> temp = new ArrayList<Double>();
		for (int i = 0; i < lowPriceList.size(); i++) {
			
			double stockPrice = (lowPriceList.get(i) + highPriceList.get(i))/2; 
			temp.add(stockPrice);
			total_average += stockPrice;
			if(i >= period - 1) {
				double total_bollinger = 0;
                double average = total_average / period;
                
                for (int x = i; x > (i - period); x--) {
                    total_bollinger += Math.pow((temp.get(x) - average), 2);
                }
                double stdev = Math.sqrt(total_bollinger / period);
                double h = (stockPrice - mean)/stdev;
                
                if (super.getCurrentPosition().equals(StrategyPosition.CLOSE)) {
                	System.out.println("Currently closed");
                	if(h < -2) {
                		super.setCurrentPosition(StrategyPosition.OPENING); 
                    	Trade newTrade = new Trade(super.ticker, super.shares, stockPrice, Action.BUY, StrategyType.BOLLINGER, this);
        				StrategyHandler.getInstance().getTrades().add(newTrade);
        				MessageHandler.initiateTrade(newTrade, "spring-beans.xml");
        				super.setLastOpenPrice(stockPrice);
                    	System.out.println("BUY");
                    	super.setCurrentAction(Action.LONG);
                    }else if(h > 2) {
                    	super.setCurrentPosition(StrategyPosition.OPENING); 
                    	Trade newTrade = new Trade(super.ticker, super.shares, stockPrice, Action.SELL, StrategyType.BOLLINGER, this);
        				StrategyHandler.getInstance().getTrades().add(newTrade);
        				MessageHandler.initiateTrade(newTrade, "spring-beans.xml");
        				super.setLastOpenPrice(stockPrice);
                    	System.out.println("SELL");
                    	super.setCurrentAction(Action.SHORT);
                    }
                }
                else if (super.getCurrentPosition().equals(StrategyPosition.OPEN)) {
    				System.out.println("currently Open");				
    				double percentage = ((i-super.getLastOpenPrice())/super.getLastOpenPrice())*100 ;
    				// if price is in between the threshold values 
    				if (percentage > 30 || percentage < -30  ) {
    					super.setCurrentPosition(StrategyPosition.CLOSE);
    				}

    			}
    			else {
    				System.out.println("NO POSITION");
    			}
			}
		}
		System.out.println("NONE");
	}
	
//	public void run() {
//		
//		 
//		System.out.println(Thread.currentThread().getName());
//		execute(StockInfo.getInstance().listLowPrices("AAPL"),StockInfo.getInstance().listHighPrices("AAPL"));
//		 
//	}
	
}
