package StrategyAlgos;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Transient;

import enums.Action;
import enums.StrategyPosition;
import jms.MessageHandler;
import magnuscapital.JPAStrategyService;
import magnuscapital.StockInfo;
import magnuscapital.Strategy;
import magnuscapital.StrategyHandler;
import magnuscapital.StrategyType;
import magnuscapital.Trade;

public class TwoMovingAvg extends Strategy {
	@Transient
	private double longAvg;
	@Transient
	private double shortAvg;
	@Transient
	private int longPeriod;
	@Transient
	private int shortPeriod;
	@Transient
	private MovingAverage l;
	@Transient
	private MovingAverage s;
	
	
	public TwoMovingAvg(int longPeriod, int shortPeriod, String ticker, long shares) {	
		super(ticker, shares);
		super.type = StrategyType.TWOMA;
		this.longPeriod  = longPeriod;
		this.shortPeriod = shortPeriod;
		longAvg  = 0;
		shortAvg = 0;
		l = new MovingAverage(longPeriod);
		s = new MovingAverage(shortPeriod);
		super.setCurrentPosition(StrategyPosition.CLOSE);
		
		StrategyHandler.getInstance().addToList(this);
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("PersistenceTest");
		EntityManager em = factory.createEntityManager();

		JPAStrategyService service = new JPAStrategyService(factory);
		Strategy s = (Strategy) this;
		System.out.println("HMMM intresting");
		
		service.add(s);
	}
	
	public void execute(ArrayList<Double>lowPriceList, ArrayList<Double>highPriceList ) {
		for (int i = 0;i<lowPriceList.size();i++) {
			double  stockPrice = (lowPriceList.get(i) + highPriceList.get(i))/2;  
			l.addData(stockPrice);
			s.addData(stockPrice);
			longAvg  = l.getMean();
			shortAvg = s.getMean();
//			System.out.println(longAvg + " " + shortAvg);
			
			if (super.getCurrentPosition().equals(StrategyPosition.CLOSE)) {
				System.out.println("Currently closed");
				if(shortAvg>longAvg) {
					
					super.setCurrentPosition(StrategyPosition.OPENING); 
					System.out.println("Opening short");
					Trade newTrade = new Trade(super.ticker, super.shares, stockPrice, Action.BUY, StrategyType.TWOMA, this);
					StrategyHandler.getInstance().getTrades().add(newTrade);					
					MessageHandler.initiateTrade(newTrade, "spring-beans.xml");
					super.setLastOpenPrice(stockPrice);
					System.out.println("BUY");
					super.setCurrentAction(Action.LONG);
		
				} else if (shortAvg<longAvg) {
					
					super.setCurrentPosition(StrategyPosition.OPENING); 
					System.out.println("Opening long");
					Trade newTrade = new Trade(super.ticker, super.shares, stockPrice, Action.SELL, StrategyType.TWOMA, this);
					StrategyHandler.getInstance().getTrades().add(newTrade);
					MessageHandler.initiateTrade(newTrade, "spring-beans.xml");
					super.setLastOpenPrice(stockPrice);
					System.out.println("SELL");
					super.setCurrentAction(Action.SHORT);
					
				}
				else {
					System.out.println("NO POSITION");
				}
			}
			else if (super.getCurrentPosition().equals(StrategyPosition.OPEN)) {
				System.out.println("currently Open");				
				double percentage = ((i-super.getLastOpenPrice())/super.getLastOpenPrice())*100 ;
				// if price is in between the threshold values 
				if (percentage > 30 || percentage < -30  ) {
					super.setCurrentPosition(StrategyPosition.CLOSE);
					System.out.println("CLOSED If open");
				}

			}
	
		}
		
	}
	public void run() {
		
		 
		System.out.println(Thread.currentThread().getName());
		execute(StockInfo.getInstance().listLowPrices("AAPL"),StockInfo.getInstance().listHighPrices("AAPL"));
		 
	}
}
