package exceptions;

import exceptions.ConflictException;
import magnuscapital.Trade;

public class ConflictByNumberException extends ConflictException {
	
	private static final long serialVersionUID = 1L;

	private int number;

	public ConflictByNumberException(int number) {
		super(Trade.class, 0);
		this.number = number;
	}

	public int getNumber() {
		return number;
	}

	@Override
	public String getMessage() {
		return "There is already an invoice with number " + number;
	}
}
