package jms;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Queue;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import enums.Action;
import enums.message;
import magnuscapital.JPATradeService;
import magnuscapital.Strategy;
import magnuscapital.StrategyHandler;
import magnuscapital.Trade;

public class MessageHandler implements MessageListener {
	
	private EntityManagerFactory factory;
	private EntityManager em;
	private FileWriter fw;
	private BufferedWriter bw;
	private PrintWriter writer;

	public void onMessage(Message m) {
		factory = Persistence.createEntityManagerFactory("PersistenceTest");
		em = factory.createEntityManager();
		JPATradeService service = new JPATradeService(factory);

		try {
			TextMessage msg = (TextMessage) m;

			fw = new FileWriter("JMSListener.txt", true);
			bw = new BufferedWriter(fw);
			writer = new PrintWriter(bw);
			writer.println("Reply: " + msg.getText());

			Trade t = null;
			for (Trade x : StrategyHandler.getInstance().getTrades()) {
				if (x.getId() == Integer.parseInt(msg.getJMSCorrelationID())) {
					t = x;
				}
			}
			
			if (t != null) {
				writer.println("trade oject isnt null");
				writer.printf("%d, %s, %s, %f, %d, %s\n\n", t.getId(), t.getAction().toString(), t.getTicker(), t.getPrice(), t.getNumShares(), t.getStrategyType().toString());
				if (msg.getText().contains("PARTIALLY")) {
					writer.println("PARTIALLY FILLED");
					int start = msg.getText().toString().indexOf("<size>") + 6;
					int end = msg.getText().toString().indexOf("</size>");
					int size = Integer.parseInt(msg.getText().toString().substring(start, end));
					t.setNumShares(size);
					service.add(t);
					t.getStrategy().setBrokerMessage(message.PARTIAL_FILL);
					t.getStrategy().setCurrentShares(size);
					writer.println("Partially FILLED from broker");
				} else if (msg.getText().contains("REJECTED")) {
					writer.println("REJECTED");
					t.getStrategy().setBrokerMessage(message.DECLINED);
					writer.println("rejected from broker");
				} else {
					writer.println("FILLED");
					service.add(t);
					t.getStrategy().setBrokerMessage(message.APPROVED);
					writer.println("approved from broker");
				}
				writer.println("calling MessageInterpreter");
				t.getStrategy().MessageInterpreter();
				
				writer.printf("%d, %s, %s, %f, %d, %s\n\n", t.getId(), t.getAction().toString(), t.getTicker(), t.getPrice(), t.getNumShares(), t.getStrategyType().toString());
				int i = StrategyHandler.getInstance().getTrades().indexOf(t);
				StrategyHandler.getInstance().getTrades().remove(i);
				
			} else {
				writer.println("NOT FOUND\n");
			}
			
		} catch (ClassCastException e) {
			e.printStackTrace();
		} catch (JMSException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			writer.close();
			factory.close();
		}
	}
	
	public static boolean initiateTrade(final Trade t, String appContext) {
		try {
			ApplicationContext context = new ClassPathXmlApplicationContext(appContext);
			Destination destination = context.getBean("destination", Destination.class);
			final Destination replyTo = context.getBean("replyTo", Destination.class);
			JmsTemplate jmsTemplate = (JmsTemplate) context.getBean("messageSender");

			jmsTemplate.send(destination, new MessageCreator() {
				public Message createMessage(Session session) throws JMSException {
					String text = "";
					if (t.getAction().equals(Action.BUY)) {
						text = "<trade>\r\n<buy>true</buy>\r\n<id>" + Integer.toString(t.getId()) + "</id>\r\n<price>"
								+ Double.toString(t.getPrice()) + "</price>\r\n<size>" + Long.toString(t.getNumShares())
								+ "</size>\r\n<stock>" + t.getTicker() + "</stock>\r\n<whenAsDate>"
								+ "2018-07-31T11:33:22.801-04:00" + "</whenAsDate>\r\n</trade>";
					} else {
						text = "<trade>\r\n<buy>false</buy>\r\n<id>" + Integer.toString(t.getId()) + "</id>\r\n<price>"
								+ Double.toString(t.getPrice()) + "</price>\r\n<size>" + Long.toString(t.getNumShares())
								+ "</size>\r\n<stock>" + t.getTicker() + "</stock>\r\n<whenAsDate>"
								+ "2018-07-31T11:33:22.801-04:00" + "</whenAsDate>\r\n</trade>";
					}
					TextMessage message = session.createTextMessage(text);
					message.setJMSReplyTo(replyTo);
					message.setJMSCorrelationID(Integer.toString(t.getId()));
					System.out.println("Message body:\n" + message.getText());
					return message;
				}
			});
			System.out.println("sent message");
			return true;
		} catch (Exception e) {
			System.out.println("can't send");
			e.printStackTrace();
			return false;
		}
	}

}
